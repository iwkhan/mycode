#!/usr/bin/python3

import urllib.request
import json

ISSNOW = "http://api.open-notify.org/iss-now.json"


def main():
    """reading json from api"""

    # call the api
    locationiss = urllib.request.urlopen(ISSNOW)

    helmet = locationiss.read()

    #print(helmet)

    helmetjson = json.loads(helmet.decode("utf-8"))

    # this should say bytes
    #print(type(helmet))


    # this should say dict
    #print(type(helmetjson))
    
    lon = helmetjson["iss_position"]["longitude"]
    lat = helmetjson["iss_position"]["latitude"]

    print(lon)
    print(lat)

    print(f"""
    CURRENT LOCATION OF THE ISS:
    Lon: {lon}
    Lat: {lat}
    """)


    #print('CURRENT LOCATION OF THE ISS:\n')
    #print("Lon:") 
    #print(helmetjson["iss_position"]["longitude"])
    #print("Lat:")
    #print(helmetjson["iss_position"]["latitude"])


if __name__ == "__main__":
    main()
